# Responsibility and the Traditional Muslim Built Environment

![](assets/images/responsibility_and_the_traditional_muslim_built_environment_cover.jpg)

This study aims to analyze the effect of the responsibility enjoyed by individuals over the built environment. 

To understand these effects the study concentrates on the physical state of the property. 

It is concluded that three claims will affect the physical state of a property: the claim of ownership, the claim of control and the claim of use. 

These three claims can be enjoyed by one or more individuals at the same time over the same property. 

A model is developed to explore the relationships between the three claims and the parties involved in sharing them, and it is then used to explain the physical state of a property. 

For example, given the same circumstances, we may expect a property that is owned, controlled and used by one person to be in a different state than if it is owned by one person, controlled by a second and used by a third. 

In the first case, responsibility is unified in one person, while in the second, it is dispersed among the three persons. 

In addition to these two, the developed model recognizes three more patterns of responsibility into which a property may be submitted.

These five states of submission of the property are called the "Forms of Submission of Property." 

The relationship between the individuals sharing the responsibility over a property will affect the state of the property. 

If the relationships between the responsible parties change, the state of the property will change. 

The relationship between responsible individuals in the traditional Muslim built environment differs from that of contemporary environments which have changed the physical state of properties. 

# Get the Book
[Direct Download ⬇️](assets/publications/books/responsibility_and_the_traditional_muslim_built_environment_thesis_phd_mit.pdf)  PDF - [Preview](https://drive.google.com/file/d/1MLdbuHgoLDDuhkRbEUomF3sD_Ax2YxxW/view?usp=drivesdk) - 473 📄

---
Thesis (Ph. D.)--MIT, Dept. of Architecture, (1984). Design Method Program.
Supervised by N. John Habraken.
Includes bibliographical references (p. 462-466).

* [Responsibility and the traditional Muslim built environment - MIT](https://dspace.mit.edu/handle/1721.1/15572)

