# Jamel Akbar Lectures

## Recorded Lectures

### Qas Al-Haq: An Alternative to Capitalism & Nation-State

Two parts discussion hosted by Ummatics

#### Part I

Human life on Earth is unsustainable if humanity continues along the same path. The dominance of the nation-state, coupled with capitalism, results in the unprecedented depletion of Earth’s resources. For instance, if humans were to live like Americans, we would require 4.8 Earths to sustain our current lifestyle and allow the planet to regenerate itself. Simultaneously, viable alternatives for humanity seem elusive when compared to Dr. Jamel Akbar paradigm-shifting book, “Qas Al-Haq.”

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/ptpjmRhCXe8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 27.04.2024
* Duration: 01:55:57

#### Part II

Following the resounding success of last month’s talk, we are thrilled to welcome back Dr. Jamel Akbar for another insightful session. Dive deeper into the alternatives to #capitalism  and the #nation-state with Dr. Akbar as he expands on his revolutionary ideas from his book, “Qas Al-Haq.”

In a world teetering on the brink of unsustainable practices, Dr. Akbar’s voice resonates as a powerful alternative. His pioneering work challenges the status quo by presenting the #Islamic legal system’s approach to #Huquq as a beacon of hope. Join us as we explore how a shift from state-centric governance to individual empowerment can pave the way for a #sustainable future.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/3BVcIzWArXM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 25.05.2024
* Duration: 02:00:25


###  End of Capitalism, Access to resources & sustainability 

This lecture was given in Milano Italy on the 20th of April 2018. It is the second lecture of three. It tries to convince Westerners of the importance of learning from other cultures to have a sustainable environment. It argues that the current western economic model, whether capitalist or socialist, will lead us to climate change and pollution no matter what. Thus, it proposes an alternative.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/TomAN24t9cQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 20.04.2018
* Duration: 01:39:16

###  Patterns of Responsibility & Quality of the Built Environment

This lecture was given in Milano Italy on the 19th of April 2018. The lecture tries to convince Westerners about the importance of learning from the traditional wisdom of the Middle East in the fields of sustainability, planning, urban design, and architecture. It is the first lecture of three. For those interested in the discussion, it starts at the 90th minute.

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/hjVI_Q5VDnI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 19.04.2018
* Duration: 02:14:04

### Sustainability Beyond Capitalism and Socialism

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/01KIyL4YtQc" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 17.06.2021
* Duration: 00:42:44


### Urbanization, Access To Resources And Sustainability

Lecture from the seminar: "The Case Of The Muslim City" Royal Institute of Art, Stockholm

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/Io6Wd_7h42A" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 07.02.2018
* Duration: 02:09:17

### Measuring Responsability & Quality of the Built Environment

Lecture from the seminar: "The Case Of The Muslim City" Royal Institute of Art, Stockholm

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/G3UB4RNNtjM" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 08.02.2018
* Duration: 02:28:13

###  Dominance, Design Process and Conventions

Lecture from the seminar: "The Case Of The Muslim City" Royal Institute of Art, Stockholm

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/wNS7s9DVv34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 09.02.2018
* Duration: 01:49:10

### Patterns of Property Ownership in Muslim Cities 

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/iEKCg4fPSBA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

* Date: 04.10.2011
* Duration: 01:16:38
