# Crisis in the Built Environment
<center>
![](assets/images/Crisis_in_the_Built_Environment_2nd_edition_conver.jpg)
</center>

## Abstract

Is it not time to search for paradigms beyond capitalism and socialism?

The book argues that interactions between the claims of “ownership”, “control” and “use” can elucidate crisis in our environments through measuring responsibilities.

Levels of responsibility created by properties’ and individuals’ rights developed by societal systems, shape our attitudes and actions in most realms of urban life such as utilization, maintenance, investments, etc.

In this edition, further arguments were added such as the impossibility of achieving sustainability and justice within socialism or capitalism.

## More about the Book
In the traditional Muslim settlement, owners and users enjoyed maximum responsibility  over  the built environment which led to maximising the best use of available space and resources.

The environment was shaped through mutual agreement and time-tested conventions, with minimal intervention from authorities. A different pattern of responsibility exists in the contemporary Muslim city.

Control is vested in the government, which maintains an orderly state of affairs through proliferating regulations.

This has led to responsibility being dispersed amongst those sharing physical elements and urban spaces resulting in many unforeseen and often hidden disastrous consequences. 

By investigating the traditional and contemporary legal systems, the decision making process, cases of disputes among neighbors, territorial structures and conventions, the
book delineates the principles that shaped the traditional environment. 

Using a constructed model of responsibility, it traces the claims of ownership, control and use and criticises the contemporary built environment. 

The book demonstrates that without considering mechanisms such as ownership and control, one can not understand the built environment and thus cannot desing it. 

It provides some serious food for thought for scholars and those involved in implementing the national development policies in the Third World.

It will also arouse the interest of the lay observer on the Muslim community life-styles past and present.

## Get The Book

* 2nd. ed. 🗐 376 - 🗓️ 2021:
    * [📖 Academia.edu](https://www.academia.edu/108976185/CRISIS_IN_THE_BUILT_ENVIRONMENT_THE_CASE_OF_THE_MUSLIM_CITY)
    * [⬇️ Download](https://drive.google.com/file/d/1eI7pqy-1qtaL7bJfvbv_T5pVYu67RwzW/view) High resolution ~240MB
    * [🖼️ Download](https://drive.google.com/drive/folders/1Jig3GjJ9NwMfOJPQ-d1RgZRI_cQKr90n?usp=sharing) Higher resolution ~1GB
    * [Google Books](https://books.google.com.tr/books/about?id=L9hhEAAAQBAJ&redir_esc=y)
    * [Google Play](https://play.google.com/store/books/details?id=L9hhEAAAQBAJ)

* 1st.  ed.  🗐 206 - 🗓️ 1988: 
    * [⬇️ Download](assets/publications/crisis_in_the_built_environment_the_case_of_the_muslim_city.pdf)
    * [📖 Preview](https://drive.google.com/file/d/14gEBOYgA-ke7ZXfCBhC2ZbVLasPH-sYO/view)

### Get The Printed Book

You can get a printed copy of the book from one of these stores:

- [Amazon](https://www.amazon.com/Crisis-Built-Environment-Case-Muslim/dp/B09P4JM2BC/ref=tmm_hrd_swatch_0?_encoding=UTF8&qid=&sr=)
- [Kitapyurdu book store](https://www.kitapyurdu.com/kitap/crisis-in-the-built-environment-second-edition-amp-the-case-of-the-muslim-city/604030.html)


## Links

- [Book citations at Google Scholar](https://scholar.google.com/citations?view_op=view_citation&hl=en&user=g0jS03wAAAAJ&citation_for_view=g0jS03wAAAAJ:u5HHmVD_uO8C)
- [Crisis in the Built Environment, Wikipedia](https://en.wikipedia.org/wiki/Crisis_in_the_Built_Environment)
