# Jamel Akbar Publications

## Jamel Akbar Books

### Crises in the Built Enviroment

![](assets/images/Crisis_in_the_Built_Environment_2nd_edition_conver_small.jpg)

The book argues that interactions between the claims of “ownership”, “control” and “use” can elucidate crisis in our environments through measuring responsibilities.

Levels of responsibility created by properties’ and individuals’ rights developed by societal systems, shape our attitudes and actions in most realms of urban life such as utilization, maintenance, investments, etc.

Visit book page for more information:

* [Crises in the Built Enviroment](Crisis_in_the_Built_Environment.md)
* 2nd. ed. 🗐 376 - 🗓️ 2021:
    * [📖 Academia.edu](https://www.academia.edu/108976185/CRISIS_IN_THE_BUILT_ENVIRONMENT_THE_CASE_OF_THE_MUSLIM_CITY)
    * [⬇️ Download](https://drive.google.com/file/d/1eI7pqy-1qtaL7bJfvbv_T5pVYu67RwzW/view) ~240MB
    * [🖼️ Download](https://drive.google.com/drive/folders/1Jig3GjJ9NwMfOJPQ-d1RgZRI_cQKr90n?usp=sharing) Higher resolution ~1GB
    * [📕 Get the printed book](Crisis_in_the_Built_Environment.md/#get-the-printed-book)
* 1st.  ed.  🗐 206 - 🗓️ 1988: 
    * [⬇️ Download](assets/publications/crisis_in_the_built_environment_the_case_of_the_muslim_city.pdf)
    * [📖 Preview](https://drive.google.com/file/d/14gEBOYgA-ke7ZXfCBhC2ZbVLasPH-sYO/view)


### Responsibility and the Traditional Muslim Built Environment

This study aims to analyze the effect of the responsibility enjoyed by individuals over the built environment. 

Visit book page for more information:

* [Responsibility and the Traditional Muslim Built Environment](Responsibility_Traditional_Muslim_Built_Environment.md)
* [⬇️ Download](assets/publications/responsibility_and_the_traditional_muslim_built_environment.pdf) - [📖 Preview](https://drive.google.com/file/d/1MLdbuHgoLDDuhkRbEUomF3sD_Ax2YxxW/view) - 🗐 473 - 🗓️ 1984


## Jamel Akbar Studies and Articles

### Property Rights (Ḥuquq) and Civilizations

As Islamic Legal System did not have the chance to perform under modern knowledge and
technology, solutions that might be created by implementing Ḥuquq are not experienced yet.
On the other hand, failure of modernity (claimant change for example) led thinkers to search
for answers in diverse disciplines in all cultures except in Ḥuquq of the Muslim World.
Moreover, humanity is searching for a system that is fair, open and transparent, a system that
is based on meritocracy. After the fall of communism, “History has ended” for some, and
thus capitalism triumphed. Liberal democracy and free market are in the lead. Some even
argues that Capitalism is a self-correcting system and that the world had accepted a political
system that is democratic, innovative and flexible and thus free market absorbs disturbances
and shocks.


* [⬇️ Download](assets/publications/property_rights_and_civilizations.pdf) - [📖 Preview](https://www.researchgate.net/publication/367052857_Property_Rights_Huquq_and_Civilizations) - 🗐 37 - 🗓️ 2023


### Support For Court-Yard Houses: Riyad, Saudi Arabia

The objective of this report is to explore the application of the support concept in the Saudi Arabian context, as a result of the author's interest in the concept of user participation. To do so, the following steps were followed.

First; an analysis and observations were made for both traditional and contemporary houses.

Second, twenty-four patterns were developed to explain the possible relationship between various patterns in the Saudi culture, and in order to clarify the capacity of the courtyard house.

Third, a support for courtyard house type was designed by using the S.A.R. methodology.

This report deals only with design aspects on the level of the individual dwelling.

* [⬇️ Download](assets/publications/support_for_courtyard_houses_riyad.pdf) - [📖 Preview](https://drive.google.com/file/d/1Wa4_eWWbUlUHXDSgWPHW2KK8cM-va1KP/view) - 🗐 116 - 🗓️ 1980

### Rights and Civilizations

Are there Islamic built environments? Through the attempt to answer the question, the paper by concentrating on the law argues that it would be impossible for Western paradigms of civilization to lead to a sustainable environment. The reason is monopolizations. 

On the other hand, the legal system of rights in Islam has the potential for prosperity whether the people are Muslims or not. Issues of decision-making processes, quality of the built environment and access to resources are discussed.

* [⬇️ Download](assets/publications/rights_and_civilizations_jamel_akbar_2019.pdf) - [📖 Preview](https://drive.google.com/file/d/1qh5E_HPH09BnYpCEfNQkrk9ecXMkmivt/view) - 🗐 21 - 🗓️ 2019 

### Interventions, Territorial Structure, Dignity And Environmental Knowiedge In The Muslim Built Environment. 

* [⬇️ Download](assets/publications/interventions_territorial_structure_dignity.pdf) - [📖 Preview](https://drive.google.com/file/d/1NrppadZGe1ItTk6cd4rHJUXP0jWNBvdL/view) - 🗐 13 - 🗓️ 2015 

### Learning from tradition: land provision and population growth, Saudi Arabia
(the case of Saudi Arabia)

Professionals in Saudi Arabia and many other Muslim countries are trying to learn from past experiences to resolve contemporary problems.

Accepting such societal strategy, this paper will illustrate one example of how history could be used to present an argument to change the attitude of decision makers, The argument is that the population of Saudi Arabia will double in the next few decades, this means that the built up area will double if the society decides to keep the same standard of living, which will cost the society. 

To minimize such cost, the paper proposes the search for a self-built system to construct infrastructure and the search for self-managed systems of controlling the built environment with the least possible intervention.

* [⬇️ Download](assets/publications/learning_from_tradition_land_provision_and_population_growth.pdf) - [📖 Preview](https://drive.google.com/file/d/1P2E-jdcukr6IfLH2ZKwniG__csycGt9B/view) - 🗐 19 - 🗓️ 2002

### Rationality.. Blight Of The Muslim Built Environment. 

Westernization is an environmental blight because of human rationality. 

After the enlightenment in the 18th century, human judgment became the demarcating criterion for all scientific and cultural developments including architectural and planning disciplines. 

The modern Muslim world followed this epistemology, although the built environment is so complex for any human rationality. 

Thus, the limitation of human rationality developed a narrow environmental knowledge that was imposed on the vivid diverse cultural and geographical regions of the Muslim world through modern and post-modern aesthetic values, industrialization, standardization, professionalism, planning codes and above all capitalism that has its own socioeconomic decision making process resulting in shallow cultural diversities. 

The paper will argue that the Islamic legal structure is distinctly different. 

In traditional environments, the Islamic legal structure generated decision making process that empowered end-users who created unlimited environmental solutions to suit their diverse cultural and geographical specificities. 

Through certain principles developed by the Islamic legal structure, those solutions were screened and then successful ones were imitated resulting in similarities and environmental typologies for each region leading to deeper cultural diversities. 

The traditional binding yet liberating Islamic legal structure (God's Wisdom as most Muslims believe) has been replaced, ironically, by a contemporary suffocating environmental systems based on human liberty and rationality.

* [⬇️ Download](assets/publications/rationality_blight_of_the_muslim_built_environment.pdf) - [📖 Preview](https://drive.google.com/file/d/13vcnQn9zuRVrZYklo-Le2Kr64OgHunTW/view) - 🗐 11 - 🗓️ 1999 


### The Merits of Cities Locations. 

"In short, this brief prologue has tried to bring attention to the need for new concepts of rights that can support and reproduce today’s advancements, beyond capitalism".

* [⬇️ Download](assets/publications/the_merits_of_cities_locations.pdf) - [📖 Preview](https://drive.google.com/file/d/1Gfi6RZIYfnIS1K6oGLFUNfzQmIXlLEzQ/view) - 🗐 8 - 🗓️ 2004

### Khatta and the Territorial Structure of Early Muslim Towns

* [⬇️ Download](assets/publications/khatta_and_the_territorial_structure_of_early_muslim_towns.pdf) - [📖 Preview](https://drive.google.com/file/d/1emtPLjo5952Q0aJ7yZVVzkPRUPAPbauA/view?usp=drivesdk) - 🗐 11 - 🗓️ 1987

### Gates as Signs of Autonomy in Muslim Towns

* [⬇️ Download](assets/publications/gates_as_signs_of_autonomy_in_muslim_towns.pdf) - [📖 Preview](https://drive.google.com/file/d/1Grheo0kLt8OdkvcSr0LbZqmcWhWJryLh/view) - 🗐 7 - 🗓️ 1993

### Accretion of Decisions: A Design Strategy. 

Theories and  Principles of Design in the Architecture of Islamic Societies.


* [⬇️ Download](assets/publications/Accretion_of_Decisions_A_Design_Strategy.pdf) - [📖 Preview](https://drive.google.com/file/d/1hdf-cceLSHMOjLyrFo0iIN2wDwGqK90Z/view) - 🗐 8 - 🗓️ 1987

### Asilah Rehabilitation On-site Review Report

Asilah Rehabilitation On-site Review Report, edited by Aga Khan Award for Architecture.

* [⬇️ Download](assets/publications/asilah_rehabilitation_onsite_review_report.pdf) - [📖 Preview](https://drive.google.com/file/d/1ymwcavqipxQebe1CZ3yYvZHf8tg36Y0i/view) - 🗐 13 - 🗓️ 1989 

### Sidi El Aloui Primary School On-site Review Report

Sidi El Aloui Primary School On-site Review Report, edited by Aga Khan Award for Architecture.

* [⬇️ Download](assets/publications/Sidi_El_Aloui_Primary_School_On-site_Review_Report_1989.pdf) - [📖 Preview](https://drive.google.com/file/d/1PFjZ34JWqKp4FdtcR-PPhoVklMMrq9Y4/view) - 🗐 13 - 🗓️ 1989 

### Ksour Rehabilitation On-site Review Report

Ksour Rehabilitation On-site Review Report, edited by Aga Khan Award for Architecture.

* [⬇️ Download](assets/publications/Ksour_Rehabilitation_On-site_Review_Report_1989.pdf) - [📖 Preview](https://drive.google.com/file/d/1SCyw0pE4QtKLOC6FkjWGS8sDwCV2z9I4/view) - 🗐 12 - 🗓️ 1989

### Entrepreneurship Development Institute of India On-site Review Report

Entrepreneurship Development Institute of India On-site Review Report, edited by Aga Khan Award for Architecture.

* [⬇️ Download](assets/publications/entrepreneurship_development_institute_of_india_report.pdf) - [📖 Preview](https://drive.google.com/file/d/1zpOjWsxGwqmCdTnzCorNqIhlFPg1LpE3/view) - 🗐 13 - 🗓️ 1992 

### Cultural Park for Children On-site Review Report

Cultural Park for Children On-site Review Report, edited by Aga Khan Award for Architecture.

* [⬇️ Download](assets/publications/cultural_park_for_children.pdf) - [📖 Preview](https://drive.google.com/file/d/11GR6VWin4hGq0Dr_Q_GnpGkV6WyfBIeE/view) - 🗐 30 - 🗓️ 1992 


### Rehabilitation of the Ksour, Draa Valley, Morocco

Rehabilitation of the Ksour, Draa Valley, Morocco. In Building for Tomorrow, edited by Azim Nanji, 74-81. London Academy Group Ltd., (1994).

❁ The On-site Review Report, formerly called the Technical Review, is a document prepared for the Aga Khan Award for Architecture by commissioned independent reviewers who report to the Master Jury about a specific shortlisted project.

The reviewers are architectural professionals specialised in various disciplines, including housing, urban planning, landscape design, and restoration.

Their task is to examine, on-site, the shortlisted projects to verify project data seek.

The reviewers must consider a detailed set of criteria in their written reports, and must also respond to the specific concerns and questions prepared by the Master Jury for each project.

This process is intensive and exhaustive making the Aga Khan Award process entirely unique.

* [⬇️ Download](assets/publications/rehabilitation_of_the_ksour_draa_valley_morocco.pdf) - [📖 Preview](https://drive.google.com/file/d/16kp15lZripL4Qu9djKJWcDjRi9-WcgA7/view) - 🗐 8 - 🗓️ 1994 

### Arabic Books

### Qas Al-Haq

Jamel Akbar's latest Arabic book (Qas al-Haq) took him more than 20 years to write (and is about 1800 pages) concentrated on economic principles by comparing rights in different cultures such as access to resources and developed conclusions regarding equity, justice, efficiency, production modes, power structures, and social settings and their ramifications on the quality of life and the built environment. 

[Qas Al-Haq](https://jamelakbar.com/قص_الحق/) - Arabic - 2nd ed. 🗐 1800 - 🗓️ 2022

### Other Arabic Books
* ‘Imarat al-’ard fi al-’Islam. Arabic Book
* Barāʼat al islām min barāʼat al-ikhtirā. Arabic Book 

