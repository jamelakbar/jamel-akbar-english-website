# Jamel Akbar - Türkçe

Profesör Doktor Jamel Akbar (Türkçe: Cemil EKBER) , İstanbul Fatih Sultan Mehmet Vakıf Üniversitesi’nde öğretim üyesi olarak görev yapmaktadır. Mimarlık lisansını 1977’de Kral Suud Üniversitesi’nde, aynı alandaki yüksek lisans ve doktorasını ise Massachusetts Teknoloji Enstitüsü’nde (MIT) 1984’te tamamlamıştır. “Yapılı Çevrede Kriz” adlı kitabın yazarıdır. Son Arapça kitabı (Qas ul-Haq) farklı kültürlerin yansımalarını karşılaştırarak ekonomik ilkeler ve kaynaklara erişim konularına odaklanan; eşitlik, adalet, verimlilik, üretim biçimleri, güç yapıları ve sosyal bağlamları ele alan çalışmaları içermektedir ve bunların yaşam kalitesi ile yapılı çevre üzerinde gözlenen sonuçlarıyla ilgilenmektedir. Kral Fahad İslam Mimarisi Ödülü’nün yanı sıra İslam Şehirleri ve Başkentleri Teşkilatı’nın birincilik ödülünü kazanmıştır.

Jamel Akbar, 14 yıl boyunca MIT Mimarlık Bölümü başkanlığını yürüten J. Habraken’in bir referans mektubunda yer alan şu ifadesinden gurur duymaktadır: “MIT’deki meslek hayatım boyunca teorik gücünü, araştırma becerileri ve yeteneği ile bu denli birleştirebilen başka bir öğrenciyle tanıştığımı sanmıyorum.” 


## Alıntılar

![Batı dünyasının ortaya çıkardığı ve bugün yüz yüze kaldığımız kentsel sorunların çözümü için tek adres İslami anlayışa dayalı bir şehircilik yaklaşımıdır](assets/images/islam_jamel_akbar_Turkce.jpg)

> Batı dünyasının ortaya çıkardığı ve bugün yüz yüze kaldığımız kentsel sorunların çözümü için tek adres İslami anlayışa dayalı bir şehircilik yaklaşımıdır. -- _Jamel Akbar_

## Kitaplar

### Qas ul-Haq (قص الحق)

* Sayfa sayısı: 📄 1800
* Dil: Arapça

![Qas ul-Haq kitap kapağı](https://jamelakbar.com/assets/images/%D8%BA%D9%84%D8%A7%D9%81_%D9%82%D8%B5_%D8%A7%D9%84%D8%AD%D9%82_%D8%B5%D8%BA%D9%8A%D8%B1.jpeg)

### Crisis in the Built Environment

![Crisis kitap kapağı](assets/images/Crisis_in_the_Built_Environment_2nd_edition_conver_small.jpg)

* [Crisis in the Built Environment - The Case of the Muslim City](https://www.kitapyurdu.com/kitap/crisis-in-the-built-environment-second-edition-amp-the-case-of-the-muslim-city/604030.html)

* Sayfa sayısı: 📄 378
* Dil: İngilizce


### Diğer kitaplar

 * Imarat al-’ard fi al-’Islam (Arapça - 📄 270)

## Ayrıca bakınız

* [Prof. Dr. Jamel AKBAR - Şehir ve Medeniyet Dergisi](https://www.sehirvemedeniyetdergisi.org/smk2022/prof-dr-jamal-akbar/)
* [Şehir ve Medeniyet Dergisi](https://twitter.com/sehirmedeniyetd/status/1594971776307679233)
* [Fatih Sultan Mehmet Vakıf Üniversitesi - Twitter](https://twitter.com/fsmvu/status/1293910180329971720)
* [Fatih Sultan Mehmet Vakıf Üniversitesi - Twitter](https://twitter.com/fsmvu/status/1165921779820441601)
* [Şehir ve Medeniyet Dergisi - Şehir ve Medeniyet Kongresi - Twitter](https://twitter.com/sehirmedeniyetd/status/1594971776307679233)
* [Şehir ve Medeniyet Dergisi - Şehir ve Medeniyet Kongresi - Twitter](https://twitter.com/sehirmedeniyetd/status/1594987344691888128)
* [FSMVÜ | Dr. Öğr. Üyesi Cemil EKBER](https://jamelakbar.fsm.edu.tr/)
* [Interventions, Territorial Structure and Environmental Knowledge in Muslim Built Environments | bab | FSMVÜ](https://dergipark.org.tr/tr/pub/babdergisi/issue/52218/683147)



