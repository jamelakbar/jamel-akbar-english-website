# Jamel Akbar

![](assets/images/banner.jpeg)

Jamel Akbar (born 26 May 1954, Taif, Hijaz) is an architect, educator, and theorist. His theoretical contributions are in the field of the built environment. His major contribution is in measuring the quality of the built environment through concepts such as responsibility, control, ownership and interventions. His work concentrates on humans' and properties’ rights among individuals, institutions and the State. By comparing such rights in different cultures he developed conclusions concerning economic and social settings and their ramifications on the quality of the built environment. 

Jamel Akbar studied architecture at King Saud University, Saudi Arabia from 1972-1977. Then he went to Massachusetts Institute of Technology from 1978-1984 where he had both M.Arch.A.S. and Ph.D. degrees. During his studies at MIT, he had the chance to teach several courses with Prof N. John Habraken and Stanford Anderson. 

## Jamel Akbar Publications

* Qas Al-Haq. So far more than 1800 pages. Arabic Book
* ‘Imarat al-’ard fi al-’Islam
* [Crises in the Built Enviroment](Crisis_in_the_Built_Environment.md)

* [Jamel Akbar's publications on Academia](https://fatihsultan.academia.edu/JamelAkbar)
* [Jamel Akbar's publications on Research Gate](https://www.researchgate.net/profile/Jamel-Akbar)

## Links

* [Twitter account](https://twitter.com/jamelakbar/)
* [Youtube channel](https://www.youtube.com/channel/UCMtCJC1GndxNWOCincSWCHQ)
* [Facebook page](https://www.facebook.com/app_scoped_user_id/10206057139465933/)
* [Telegrm](https://t.me/jamelakbar)
* [Research Gate](https://www.researchgate.net/profile/Jamel-Akbar)
* [Academia](https://fatihsultan.academia.edu/JamelAkbar)
* [Jamel Akbar, Wikipedia](https://en.wikipedia.org/wiki/Jamel_Akbar)
* [Jamel Akbar, Google Scholar](https://scholar.google.com/citations?user=g0jS03wAAAAJ)
* [ORCID 0000-0003-0685-0500](https://orcid.org/0000-0003-0685-0500)
